jQuery( document ).ready(function( $ ) {
		var windowH;
		var overallTextH;
		var bigTextOffset;
		var navbarOffset = $("#big-home-nav").offset().top + $("#big-home-nav").outerHeight() + 60;
		var bigLogoPos = $("#big-cyl-logo").offset().top;
		var scrollY;
		var trashPositions =[];
		var broomPosition;
		var bigStmntMargin;
		var bigStmntHt;
		var mobileTrash;
		var cleanup1Position;
		
		
		
		
		$(window).load(function() {
			windowH = $(this).height(); 
/*
			overallTextH = $("#big-statement-container").height();
			bigStmntMargin = (windowH - overallTextH)/1.65;
			$("#big-statement-container").css("margin-top", bigStmntMargin);
			bigTextOffset = $("#big-cleanup-statement").offset().top;
			bigStmntHt = $("#big-cleanup-statement").outerHeight();
*/			broomPosition = $("#cleanup-broom").offset().left;
			console.log(broomPosition);
			
			
/*
			$("#cleanup-overlay-1").offset({top: ((bigTextOffset+(bigStmntHt/4)))});
			$("#cleanup-overlay-2").offset({top: ((bigTextOffset+(bigStmntHt-($("#cleanup-overlay-2").outerHeight()/2))))});
			$("#cleanup-overlay-3").offset({top: ((bigTextOffset+(bigStmntHt-($("#cleanup-overlay-3").outerHeight()/1.5))))});
			$("#cleanup-overlay-4").offset({top: ((bigTextOffset+((bigStmntHt/2)-($("#cleanup-overlay-4").outerHeight()/1.75))))});
			
*/
			$("#mobile-trash-overlay").css({height: (bigStmntHt*1.25)});
			mobileTrash = $("#mobile-trash-overlay").width();
			$("#mobile-trash-overlay").css({left: (($(window).width()/2)-(mobileTrash/2))});
			$("#mobile-trash-overlay").offset({top: (bigTextOffset-20)});
			$( ".cleanup-overlay" ).each(function( index, element ) {
				trashPositions.push($(element).offset().top);
			});
			
		});
		
		$(window).resize(function() {
			windowH = $(this).height();
			broomPosition = $("#cleanup-broom").offset().left;
			overallTextH = $("#big-cleanup-statement-container").height();
			bigStmntMargin = (windowH - overallTextH)/1.75;
			$("#big-cleanup-statement-container").css("margin-top", bigStmntMargin);
			bigTextOffset = $("#big-cleanup-statement").offset().top;
			bigStmntHt = $("#big-cleanup-statement").outerHeight();
			
			$("#mobile-trash-overlay").css({height: (bigStmntHt*1.25)});
			mobileTrash = $("#mobile-trash-overlay").width();
			$("#mobile-trash-overlay").css({left: (($(window).width()/2)-(mobileTrash/2))});
			$("#mobile-trash-overlay").offset({top: (bigTextOffset-20)});
			console.log(broomPosition);
			$( ".cleanup-overlay" ).each(function( index, element ) {
				trashPositions.push($(element).offset().top);
			});
		});
		
		
		$(window).scroll(function() {
			scrollY = $(this).scrollTop();
			console.log(scrollY);
			if (scrollY > navbarOffset) {
				//console.log("scrolled past it");
				$(".navbar-default").addClass("scrolled");
			} else {
				$(".navbar-default").removeClass("scrolled");
			}
			$("#big-cyl-logo").offset({top: (bigLogoPos-(scrollY/navbarOffset))});
			$("#big-home-nav").css("opacity", (1-(scrollY/navbarOffset)));
			$("#cleanup-broom").css("left", (broomPosition+scrollY));
			
			$(".cleanup-overlay").each(function( index, element ) {
				//$(element).offset({top: (trashPositions[index]-scrollY)});
			//	$(element).css("opacity", (1-(scrollY/navbarOffset)));
			});
			
		});
	});
