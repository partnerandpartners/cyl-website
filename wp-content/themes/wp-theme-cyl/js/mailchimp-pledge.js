(function($) {
  $(document).ready(function() {

    window.location.hash = '';

    function at_least_one_commitment_isnt_selected() {
      console.log($('form#pledge-form').serialize());
      return $('form#pledge-form').serialize().indexOf('interests') == -1;
    }


    function go_to_target( target ) {
      if( $( target ).css('display') == 'none' ) {
        $( target ).slideDown();
      }

      var newScrollPosition = $(target).closest('.pledge-panel-wrapper').position().top;
      $('html, body').stop().animate({scrollTop: newScrollPosition}, 400);
    }


    $('input[name="MMERGE4"]').change(function(){
      $('input[name="MMERGE7"]').val( $(this).val() );
    })

    $('.pledge-panel-link').click(function(e){
      e.preventDefault();

      var target = $(this).attr('href');
      // go_to_target(target);

      if( target == '#your-business-content' ) {
        if( at_least_one_commitment_isnt_selected() ) {
          //alert('Please select one commitment.')
          $( "p#commitment-error-msg" ).text( "Please select at least one commitment." );
        } else {
          go_to_target(target);
        }
      } else if( target == '#sign-pledge-content' ) {
        if( !your_business_is_done() ) {
          //alert('Please fill out the form.')
          $( "p#your-business-error-msg" ).text( "Please complete the form." );
        } else {
          go_to_target(target);
        }
      } else {
        go_to_target(target)
      }

    })

    $('#pledge-form input[type="checkbox"]').change(function(){
      $('#pledges').empty()

      $('#pledge-form input:checked').each(function(index, pledgeInput){
        var pledgeText = $(pledgeInput).parent('label').text().trim()

        var checkedCheckbox = $('<div>').addClass('clearfix').append( $('<input>').attr('type','checkbox').attr('checked','checked') ).append( $('<div>').text(pledgeText) );
        $('#pledges')
          .append( checkedCheckbox );
      })
    })

    $('#close-modal').click(function(e){
      e.preventDefault();
      $('#pledge-overlay').removeClass('visible');
      $(this).trigger('reset');

      window.location.hash = 'clean-pledge';
      window.location.reload()
    })


    function your_business_is_done() {
      var inputs_filled = true;
      $('#your-business input').each(function(){
        if( $(this).val().trim().length == 0 ) {
          inputs_filled = false;
        }
      })

      return inputs_filled;
    }

    $('#pledge-form').submit(function(e){
      e.preventDefault();

        // check that at least one interest exists
        var serializedFormArray = $(this).serializeArray();
        var interest_exists = false;
        var signature_exists = false;
        var date_exists = false;
        serializedFormArray.forEach(function(field){
          if(field.name == 'interests[]') {
            interest_exists = true;
          }
        })
        if ($('input[name="MMERGE5"]').val().length != 0) {
          signature_exists = true;
        }
        if ($('input[name="MMERGE6"]').val().length != 0) {
          date_exists = true;
        }
        // console.log($('input[name="MMERGE5"]').val().length);
        // console.log($('input[name="MMERGE6"]').val().length);

        if( !interest_exists ) {
          console.log('nope');
          $('#commitments .pledge-panel').slideDown();

          var showing_alert = false;

          var newScrollPosition = $('#commitments').position().top;
          $('html, body').stop().animate({scrollTop: newScrollPosition}, 400, function(){
            if( !showing_alert ) {
              alert('Please select at least one commitment.')
              showing_alert = true;
            }
          });
        } else if( !signature_exists ) {
          console.log('signature=');
          console.log($('input[name="MMERGE5"]').val());
        } else if( !date_exists ) {
          console.log('date=');
          console.log($('input[name="MMERGE6"]').val());
        } else {
          if(typeof mailchimp_pledge != undefined) {

            $('#pledge-overlay').addClass('visible');

            $.ajax({
              method: 'POST',
              url: mailchimp_pledge.ajax_url,
              data: $('#pledge-form').serialize(),
              success: function(data) {
                console.log(data);
              }
            })
          }
        }
    })
  })
})(jQuery);
